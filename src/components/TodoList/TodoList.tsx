import React, { useEffect, useRef } from 'react';
import './TodoList.scss'
import { Todo } from '../../interfaces/Todo.interface';

interface todoListProps {
    todoList: Todo[];
    setTodoList: React.Dispatch<React.SetStateAction<Todo[]>>;
}

const TodoList = ({ todoList, setTodoList }: todoListProps) => {

    const bottomRef = useRef<HTMLDivElement>(null);

    const todoClass = (t: Todo) => {
        if (t.isImportant && t.isUrgent) {
            return 'todo-card--red'
        } else if (t.isImportant) {
            return 'todo-card--orange'
        } else if (t.isUrgent) {
            return 'todo-card--yellow'
        }
        return 'todo-card--green'
    };

    useEffect(() => {
        // 👇️ scroll to bottom every time messages change
        bottomRef.current?.scrollIntoView({behavior: 'smooth'});
      }, [todoList]);

    return (
        <div className='row row-fill-container mb-6'>

            <div className='todo-list-card col-10 elevation-2 mh-6'>
                <div className='scrollable'>
                    <div className='col-20'>
                        {todoList.map((t, i) => (
                            <div 
                            className={`todo ${todoClass(t)} elevation-1 col-20 mv-3`} key={i}>
                                <div>{t.todoText}</div>
                                <div className='spacer'></div>
                                <div className="material-symbols-outlined">edit</div>
                                <div className="material-symbols-outlined">check</div>
                                <div className="material-symbols-outlined">delete</div>
                            </div>
                        ))}
                    </div>
                    <div ref={bottomRef} />
                </div>
            </div>

        </div>
    );
}

export default TodoList;
