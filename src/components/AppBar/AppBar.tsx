import React from 'react';
import './AppBar.scss'
import BreakPoint from "../../assets/styles/BreakPoint";

const AppBar = () => {
  return (
    <nav className='app-bar'>

      <BreakPoint fromXsTo="md">
        <button className='app-bar__menu-button'>
          <span className='material-symbols-outlined'>
            menu
          </span>
        </button>
        <div className='app-bar__title--centered'>
          ListApp Pendientes
        </div>
      </BreakPoint>

      <BreakPoint fromXlhTo="mdh">
        <div className='app-bar__title'>
          ListApp Pendientes
        </div>

        <ul className='app-bar__tool-bar'>
          <li className="tool-bar__link"><a href="/">Link 1</a></li>
          <li className="tool-bar__link"><a href="/">Link 2</a></li>
          <li className="tool-bar__link"><a href="/">Link 3</a></li>
        </ul>

        <div className='app-bar__spacer'></div>

        <ul className="app-bar__tool-bar">
          <li className="tool-bar__link"><a href="/">act1</a></li>
          <li className="tool-bar__link"><a href="/">act2</a></li>
          <li className="tool-bar__icon-button">
            <span className='material-symbols-outlined'>
              notifications
            </span>
          </li>
          <li className="tool-bar__button">
            <span className='material-symbols-outlined'>
              person
            </span>
            <span>
              UserName
            </span>
          </li>
        </ul>
      </BreakPoint>
    </nav>
  )
}

export default AppBar;