import React, { FormEvent, useState } from 'react';
import './App.scss';
import AppBar from './components/AppBar/AppBar';
import TodoInput from './components/TodoInput/TodoInput';
import TodoList from './components/TodoList/TodoList';
import { Todo } from './interfaces/Todo.interface';


function App() {

  const [todo, setTodo] = useState<Todo>({ id: 0, todoText: '', isDone: false, isImportant: false, isUrgent: false });
  const [todoList, setTodoList] = useState<Todo[]>([]);

  const hadleAddTodo = (e: FormEvent) => {
    e.preventDefault();

    if (todo.todoText != '') {
      setTodoList([...todoList, {
        id: Date.now(),
        todoText: todo.todoText,
        isDone: false,
        isImportant: todo.isImportant,
        isUrgent: todo.isUrgent
      }]);
      setTodo({
        id: 0, todoText: '',
        isDone: false,
        isImportant: false,
        isUrgent: false
      });

      
    }
  };

  return (
    <div className="App">
      <header>
        <AppBar></AppBar>
      </header>
      <main>
        <aside className='elevation-2'>
          <ul>
            <li>item 1</li>
            <li>item 2</li>
            <li>item 3</li>
            <li>item 4</li>
            <li>item 5</li>
            <li>item 6</li>
            <li>item 7</li>
            <li>item 8</li>
            <li>item 9</li>
            <li>item 10</li>
          </ul>
        </aside>
        <section className='main-section'>
          <TodoInput
            todo={todo}
            setTodo={setTodo}
            handleAddTodo={hadleAddTodo}></TodoInput>
          <TodoList todoList={todoList} setTodoList={setTodoList} ></TodoList>
        </section>
      </main>
      <footer>

      </footer>
    </div>
  );
}

export default App;
