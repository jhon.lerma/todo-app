export interface Todo{
    id: number;
    todoText: string;
    isDone: boolean;
    isImportant: boolean;
    isUrgent: boolean;
}