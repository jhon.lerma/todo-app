import React, { FormEvent, useRef } from 'react';
import './TodoInput.scss'
import { Todo } from '../../interfaces/Todo.interface';

interface TodoInputProps {
    todo: Todo;
    setTodo: React.Dispatch<React.SetStateAction<Todo>>;
    handleAddTodo: (e: FormEvent) => void;
}

const TodoInput = ({ todo, setTodo, handleAddTodo }: TodoInputProps) => {

    const inputTodoTextRef = useRef<HTMLInputElement>(null);

    return (
        <div className="row">
            <div className='card elevation-1 col-10 m-6'>
                <form
                    onSubmit={(e) => {
                        handleAddTodo(e);
                        inputTodoTextRef.current?.focus();
                    }}>
                    <div className='row'>
                        <div className='form-input-control col-20'>
                            <input
                                ref={inputTodoTextRef}
                                className='form-input-control__text-input'
                                type='text'
                                name='todoInputField'
                                id='todoInputField'
                                placeholder='Introduzca una tarea'
                                value={todo.todoText}
                                onChange={(e) => setTodo({ ...todo, todoText: e.target.value })}
                            />
                            <label className='form-input-control__text-input-label' htmlFor='todoInputField'>
                                Texto tarea
                            </label>
                        </div>
                    </div>

                    <div className="row">
                        <div className='form-checkbox-control'>
                            <label className='form-checkbox-control__checkbox'>
                                <input
                                    type='checkbox'
                                    name='checkboxIsImportant'
                                    id='checkboxIsImportant'
                                    checked={todo.isImportant}
                                    onChange={(e) => setTodo({ ...todo, isImportant: e.target.checked })}
                                />
                                <span className="checkbox__checkmark material-symbols-outlined"></span>
                                <span className='checkbox__label'>Importante</span>
                            </label>
                        </div>
                        <div className='form-checkbox-control'>
                            <label className='form-checkbox-control__checkbox'>
                                <input
                                    type='checkbox'
                                    name='checkboxIsUrgent'
                                    id='checkboxIsUrgent'
                                    checked={todo.isUrgent}
                                    onChange={(e) => setTodo({ ...todo, isUrgent: e.target.checked })}
                                />
                                <span className="checkbox__checkmark material-symbols-outlined"></span>
                                <span className='checkbox__label'>Urgente</span>
                            </label>
                        </div>
                        {/* <button className='form-icon-button__icon-button mgl-2' type='submit'>
                    <span className='material-symbols-outlined'>
                        add
                    </span>
                </button> */}

                        <div className="spacer ml-2 display-lg-none"></div>

                        <button className='form-button elevation-1 col-lg-20' type='submit'>
                            Agregar tarea
                            <span className='material-symbols-outlined'>
                                add
                            </span>
                        </button>

                    </div>
                </form>

            </div>

        </div>
    )
}

export default TodoInput;