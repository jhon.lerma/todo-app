import { useEffect, useState } from "react";

const breakPoints = {
  xs: '(max-width:320px)',
  xsh: '(max-width:375px)',
  sm: '(max-width:425px)',
  smh: '(max-width:600px)',
  md: '(max-width:768px)',
  mdh: '(max-width:896px)',
  lg: '(max-width:1024px)',
  lgh: '(max-width:1232px)',
  xl: '(max-width:1440px)',
  xlh: '(min-width:1440px)',
} as const

type MediaBreakPoint = keyof typeof breakPoints;

const useMediaQuery = (query: MediaBreakPoint) => {
  const [matches, setMatches] = useState(false);

  useEffect(() => {
    const mediaQuery = window.matchMedia(breakPoints[query]);
    setMatches(mediaQuery.matches);    

    mediaQuery.addEventListener('change', (event) => {
      setMatches(event.matches);      
    });

    return () =>
      mediaQuery.removeEventListener('change', (event) => {
        setMatches(event.matches);
      });
  }, [query]);

  return matches;
}

export default useMediaQuery;