import useMediaQuery from './useMediaQuery';

export default function useBreakpoints() {
    const breakPoints = {
        isXs: useMediaQuery('xs'),
        isXsh: useMediaQuery('xsh'),
        isSm: useMediaQuery('sm'),
        isSmh: useMediaQuery('smh'),
        isMd: useMediaQuery('md'),
        isMdh: useMediaQuery('mdh'),
        isLg: useMediaQuery('lg'),
        isLgh: useMediaQuery('lgh'),
        isXl: useMediaQuery('xl'),
        isXlh: useMediaQuery('xlh'),
        list:['xs', 'xsh', 'sm', 'smh', 'md', 'mdh', 'lg', 'lgh', 'xl', 'xlh'],
        active: 'xs'
    };
    if (breakPoints.isXs) breakPoints.active = 'xs'
    else if (breakPoints.isXsh) breakPoints.active = 'xsh'
    else if (breakPoints.isSm) breakPoints.active = 'sm'
    else if (breakPoints.isSmh) breakPoints.active = 'smh'
    else if (breakPoints.isMd) breakPoints.active = 'md'
    else if (breakPoints.isMdh) breakPoints.active = 'mdh'
    else if (breakPoints.isLg) breakPoints.active = 'lg'
    else if (breakPoints.isLgh) breakPoints.active = 'lgh'
    else if (breakPoints.isXl) breakPoints.active = 'xl'
    else if (breakPoints.isXlh) breakPoints.active = 'xlh'
    
    return breakPoints;
}