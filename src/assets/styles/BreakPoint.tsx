import useBreakPoints from "./useBreakPoints";

type BreakPointProps = {
    at?: string;
    fromXlhTo?: string;
    fromXsTo?: string;
    fromTo?:[string, string];
    children: any;
};


const BreakPoint = (props: BreakPointProps) => {

    const { active, list } = useBreakPoints();


    if (props.at && !props.fromXsTo && !props.fromXlhTo && !props.fromTo) {

        return active === props.at ? props.children : null;

    } else if (!props.at && props.fromXsTo && !props.fromXlhTo && !props.fromTo) {

        return (list.indexOf(active) > -1 && list.indexOf(props.fromXsTo) >= list.indexOf(active)) ? props.children : null;

    } else if (!props.at && !props.fromXsTo && props.fromXlhTo && !props.fromTo) {

        return (list.indexOf(active) > -1 && list.indexOf(props.fromXlhTo) <= list.indexOf(active)) ? props.children : null;

    } else if (!props.at && !props.fromXsTo && !props.fromXlhTo && props.fromTo) {

        // console.log(`active: ${active} index props.fromTo[0]: ${list.indexOf(props.fromTo[0])} index props.fromTo[1]: ${list.indexOf(props.fromTo[1])} list indexof active: ${list.indexOf(active)}`);

        return (list.indexOf(active) > -1 && list.indexOf(props.fromTo[0]) <= list.indexOf(active) && list.indexOf(props.fromTo[1]) >= list.indexOf(active)) ? props.children : null;
    }

    console.error('<BreakPoint>: debe especificar un breakpoint para alguna de las propiedades (at, fromXlhTo, fromXsTo, fromTo).');
    return null;
}

export default BreakPoint;